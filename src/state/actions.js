import fetch from 'fetch-jsonp'
import moment from 'moment';

export function getPopularMovies () {
  return dispatch => {
    const fourStarUrl = 'https://itunes.apple.com/search?country=us&media=movie&entity=movie&limit=100&attribute=ratingIndex&term=4'
    const fiveStarUrl = 'https://itunes.apple.com/search?country=us&media=movie&entity=movie&limit=100&attribute=ratingIndex&term=5'
    const req1 = fetch(fourStarUrl)
    const req2 = fetch(fiveStarUrl)

    return Promise.all([req1, req2])
      .then(responses => responses.map(res => res.json()))
      .then(jsonPromises => Promise.all(jsonPromises))
      .then(jsonResponses => {
        //
        // jsonResponses contains the results of two API requests
        //

        //
        // 1. combine the results of these requests
        // 2. sort the results FIRST by year THEN by title (trackName)
        // 3. each movie object in the results needs a releaseYear attribute added
        //    this is used in src/components/movies-list.js line 26
        //
        
        //jsonResponses => [ [4 star movies],[5 star movies]  ]

        jsonResponses.forEach((jsonResponse,index)=>{
                
                //jsonResponse =[ movie1, movie2 .... ]
                jsonResponse.results.forEach(movie=>{
                    //movie = {title. ...   }
                    movie.rating = index=== 0 ? 4 : 5;
                    var releaseYear = movie.releaseDate;
                    movie.releaseMoment = moment(releaseYear,moment.ISO_8601);
                    movie.releaseYear = movie.releaseMoment.get("year");
                });
                return jsonResponses;
        });
        // [[movies],[movies]] // just that each movie has 3 extra keys
        
          
        
        const combinedResults = [].concat(jsonResponses[0].results).concat(jsonResponses[1].results);
        
        //combinedResults = [movies] //all 4 star and 5 star movies are now in the same array
        
        //Map
        const resultYearMappings = new Map();
        
        
        combinedResults.forEach(result=>{
            //result => movie
            if(!resultYearMappings.get(result.releaseYear)){
              //{ 2015 : [] }
              resultYearMappings.set(result.releaseYear,[]);
            }
            //{ 2012 : [...], 2015: [result]}
            resultYearMappings.get(result.releaseYear).push(result);
        });
        
        //{2012: [movies], 2015:[movies] ....}

        window.results = combinedResults;
        window.resultYearMappings = resultYearMappings;

        resultYearMappings.forEach((_results,key)=>{
            //  _results => movies, key => 2012
            
            _results.sort((_result1,_result2)=>{
              var textA = _result1.trackName.toUpperCase();
              var textB = _result2.trackName.toUpperCase();
              return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
            
            //_results => array of movies sorted by title
            var _titles = _results.map(_result=>_result.trackName);
            console.log(_titles);
        });
        
        //map => {
        //  2012: [movies sorted by title],
        // ....
        //}
         
        //resultYearMappings.keys() => [2012,2014,1997 ...]
        const resultYearKeys = Array.from(resultYearMappings.keys()).sort();
        //resultYearKeys = [1997,2012,2014 ....];
        
        let sortedResults = [];
        
        
        //resultYearMappings.get(key) => [[1997 movies],[2012 movies]....]
        resultYearKeys.map(key=>resultYearMappings.get(key)).forEach(moviesByYear=>{
          //[].concat[1997 movies]
          sortedResults = sortedResults.concat(moviesByYear);
        });
        
        //[movies sorted by year first and title later]
        
        return dispatch({
          type: 'GET_MOVIES_SUCCESS',
          movies: sortedResults
        });
      })
  }
}
